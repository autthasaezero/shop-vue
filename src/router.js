import Vue from 'vue'
import Router from 'vue-router'
import DashboardLayout from '@/layout/DashboardLayout'
Vue.use(Router)

export default new Router({
  linkExactActiveClass: 'active',
  mode: 'history',
  routes: [
    {
      path: '/',
      redirect: 'dashboard',
      component: DashboardLayout,
      children: [
        {
          path: '/',
          name: 'shops',
          component: () => import(/* webpackChunkName: "demo" */ './views/reports/Shop.vue')
        },
        {
          path: '/products',
          name: 'products',
          component: () => import(/* webpackChunkName: "demo" */ './views/reports/ReportProduct.vue')
        },
      ]
    },
  ]
})