import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './stores/store'
import './registerServiceWorker'
import ArgonDashboard from './plugins/argon-dashboard'
import BootstrapVue from 'bootstrap-vue'
import Notifications from 'vue-notification'
import moment from 'moment'
import { email as RuleEmail }from 'vee-validate/dist/rules'
import { ValidationProvider } from 'vee-validate'
import { ValidationObserver } from 'vee-validate'
import VueClipboard from 'vue-clipboard2'
import VCalendar from 'v-calendar'
import * as VueSpinnersCss from "vue-spinners-css";
import DisableAutocomplete from 'vue-disable-autocomplete';
import VModal from 'vue-js-modal'
import vueNumeralFilterInstaller from 'vue-numeral-filter';
import VueResource from 'vue-resource';
import VueFilterDateFormat from 'vue-filter-date-format';
import JsonExcel from 'vue-json-excel';

// import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'
import { i18n } from './i18n'
import { extend } from 'vee-validate';
import * as rules from 'vee-validate/dist/rules';
import { localize } from 'vee-validate';
import en from 'vee-validate/dist/locale/en.json';
import th from 'vee-validate/dist/locale/th.json';
// import { ModalPlugin } from 'bootstrap-vue'

Vue.use(VueResource)
Vue.use(ArgonDashboard)
Vue.use(BootstrapVue)
Vue.use(require('typy'))
// Vue.use(ModalPlugin)
Vue.use(Notifications)
Vue.use(VueClipboard)
Vue.use(VCalendar)
Vue.use(require('vue-moment'))
Vue.use(VueSpinnersCss)
Vue.use(DisableAutocomplete);
Vue.use(VModal);
Vue.component('ValidationProvider', ValidationProvider)
Vue.component('ValidationObserver', ValidationObserver)
Vue.component('downloadExcel', JsonExcel)
Vue.use(vueNumeralFilterInstaller);
Vue.use(VueFilterDateFormat);

// แปลภาษา validation
localize({
  en,
  th
});

Object.keys(rules).forEach(rule => {
  extend(rule, rules[rule]);
});

// with typescript
for (let [rule, validation] of Object.entries(rules)) {
  extend(rule, {
    ...validation
  });
}

Vue.config.productionTip = false
Vue.prototype.moment = moment

Vue.mixin({
  data() {
    return {
      APP_VERSION: '0.1.0',
      DEFAULT_ACTIVE_USER: true,
      REQUIRE_ACTIVE_USER: false,
      mainLoading: false
    }
  },
  methods: {
    setLocale (locale) {
      this.$i18n.locale = locale
    },
    showModalLoading () {
      this.mainLoading = true;
    },
    hideModalLoading () {
      this.mainLoading = false;
    }
  }
})

localize('th');
extend('phoneOrEmailRule', (value) => {
  const mobileReg = /^(\d{10})$/;
  if (RuleEmail.validate(value) || mobileReg.test(value)) {
    return true;
  }

  return i18n.t('login.phoneNumberOrE-mail'); //Must be either a valid phone number or e-mail
});

Vue.filter('capitalize', function (value) {
  if (!value) return ''
  value = value.toString()
  return value.charAt(0).toUpperCase() + value.slice(1)
})
console.log(process.env.VUE_APP_MLM_API_ENDPOINT)

new Vue({
  i18n,
  router,
  store,
  render: h => h(App),
}).$mount('#app')
