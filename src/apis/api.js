import axios from 'axios'
var apiTimeOut = process.env.VUE_APP_API_TIMEOUT;

export default (config) => {
  let configAxios = {
    baseURL: null,
    withCredentials: false,
    timeout: apiTimeOut,
    headers: {
    }
  }
  configAxios = {...configAxios, ...config}
  const instance = axios.create(configAxios)
  instance.interceptors.request.use(function (config) {
    return config
  }, function (error) {
    return Promise.reject(error)
  })

  instance.interceptors.response.use(function (response) {
    return response
  }, function (error) {
    return Promise.reject(error)
  })

  return instance
}