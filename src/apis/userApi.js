import axios from 'axios'
import Api from './api'
Api.baseUrl = 'http://localhost'
Api.apiTimeOut = 10000
const baseUrl = 'http://localhost'

export default {
  getShop (payload) {
    return axios.get(`${baseUrl}/api/v1/shops/`, {params: payload});
  },
  getShopById (payload) {
    return axios.get(`${baseUrl}/api/v1/shops/` + payload.id);
  },
  addShop (payload) {
    return axios.post(`${baseUrl}/api/v1/shops/`, payload);
  },
  updateShop (payload) {
    return axios.put(`${baseUrl}/api/v1/shops/`+payload.id, payload);
  },
  getProduct (payload) {
    return axios.get(`${baseUrl}/api/v1/products/`, {params: payload});
  },
  getProductByShop (payload) {
    return axios.get(`${baseUrl}/api/v1/shops/` + payload.id + '/product');
  },
  addProduct (payload) {
    return axios.post(`${baseUrl}/api/v1/products/`, payload);
  },
  updateProduct (payload) {
    return axios.put(`${baseUrl}/api/v1/products/`+payload.id, payload);
  },
  deleteProduct (payload) {
    return axios.delete(`${baseUrl}/api/v1/products/`+payload.id);
  },
  getProductCategory () {
    return axios.get(`${baseUrl}/api/v1/product_categories/`);
  },
}

