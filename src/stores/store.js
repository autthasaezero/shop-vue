import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios'
import userStore from './userStore'

Vue.use(Vuex)
axios.defaults.baseURL = process.env.VUE_APP_API
axios.defaults.timeout = 15000
// const API_SERSION = process.env.VUE_APP_API_VERSION

let isAlreadyFetchingAccessToken = false
var subscribers = []

axios.interceptors.request.use(function (config) {
  if (localStorage.getItem('accessToken')) {
    config.headers.common['Authorization'] = 'Bearer ' + localStorage.getItem('accessToken')
  }
  return config
}, function (error) {
  return Promise.reject(error)
})

axios.interceptors.response.use(function (response) {
  let url = response.config.url
  let status = 'status' in response.data ? response.data.status.code : null
  if (status === 401 && url.search('password-reset') === -1) {
    return resetTokenAndReattemptRequest(response)
  } else {
    return response
  }
}, function (error) {
  let url = error.config.url
  if (!error.response && url.search('oauth/token') === -1) {
    return resetTokenAndReattemptRequest(error)
  }
  return Promise.reject(error)
})

function resetTokenAndReattemptRequest (res) {
  try {
    const retryOriginalRequest = new Promise(resolve => {
      addSubscriber(() => {
        res.config.headers.Authorization = 'Bearer ' + localStorage.getItem('accessToken')
        resolve(axios(res.config))
      })
    })
    if (!isAlreadyFetchingAccessToken) {
      isAlreadyFetchingAccessToken = true
      axios.post('/oauth/token', {
        grant_type: 'refresh_token',
        client_id: process.env.VUE_APP_CLIENT_ID,
        refresh_token: localStorage.getItem('refreshToken'),
        client_secret: process.env.VUE_APP_SECRET,
        scope: '*'
      }).then(response => {
        localStorage.setItem('accessToken', response.data.access_token)
        localStorage.setItem('refreshToken', response.data.refresh_token)
        onAccessTokenFetched(response.data.access_token)
      }).catch(error => {
        return Promise.reject(error)
      })
    } else {
      window.location.href = '/logout'
    }
    return retryOriginalRequest
  } catch (err) {
    return Promise.reject(err)
  }
}

function onAccessTokenFetched (accessToken) {
  subscribers.forEach(callback => callback(accessToken))
  subscribers = []
}

function addSubscriber (callback) {
  subscribers.push(callback)
}

export default new Vuex.Store({
  modules: {
    user: userStore,
  },
  state: {
    accessToken: localStorage.getItem('accessToken') || null,
    refreshToken: localStorage.getItem('refreshToken') || null,
    language: localStorage.getItem('language') || process.env.VUE_APP_DEFAULT_LANGUAGE,
    languages: [
      {
        'key': 'th',
        'name': 'ภาษาไทย'
      },
      {
        'key': 'en',
        'name': 'English'
      },
    ]
  },
  getters: {
    languages: state => {
      return state.languages
    },
    currentLanguage: state => {
      return state.language
    },
    currentLanguageName: state => {
      const currentLang = state.languages.filter(lang => lang.key === state.language)
      return  currentLang.length == 1 ? currentLang[0].name : 'English'
    }
  },
  mutations: {
    changeLanguage (state, language) {
      state.language = language
    }
  },
  actions: {
    changeLanguage (context, language) {
      localStorage.setItem('language', language)
      context.commit('changeLanguage', language)
    }
  }
})
