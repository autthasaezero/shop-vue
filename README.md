# mlm-frontend

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Run develop
```
npm run develop
```

### SCREEN
[![API docs](public/img/sceen/01.png)]
[![API docs](public/img/sceen/02.png)]
[![API docs](public/img/sceen/03.png)]
[![API docs](public/img/sceen/04.png)]
[![API docs](public/img/sceen/05.png)]


